# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{BUILD_DIR}  = /media/debuilder/
@{BUILD_DIR} += /tmp/kernel-build-morfik/
@{BUILD_DIR} += /usr/src/

@{exec_path} = /{usr/,}bin/debsign
profile debsign @{exec_path} {
  include <abstractions/base>

  @{exec_path} r,
  /{usr/,}bin/{,ba,da}sh rix,

  /{usr/,}bin/mktemp     rix,
  /{usr/,}bin/basename   rix,
  /{usr/,}bin/{,e}grep   rix,
  /{usr/,}bin/mv         rix,
  /{usr/,}bin/cat        rix,
  /{usr/,}bin/cp         rix,
  /{usr/,}bin/rm         rix,
  /{usr/,}bin/head       rix,
  /{usr/,}bin/cu         rix,
  /{usr/,}bin/cut        rix,
  /{usr/,}bin/stty       rix,
  /{usr/,}bin/sed        rix,
  /{usr/,}bin/getopt     rix,
  /{usr/,}bin/dirname    rix,
  /{usr/,}bin/cmp        rix,

  /{usr/,}bin/md5sum     rix,
  /{usr/,}bin/sha{1,256,512}sum rix,

  /{usr/,}bin/perl       rix,

  @{etc_ro}/devscripts.conf r,
  owner @{HOME}/.devscripts r,

  # For package building
  owner @{BUILD_DIR}/** rwkl -> @{BUILD_DIR}/**,

  owner /tmp/debsign.*/ rw,
  owner /tmp/debsign.*/*.{dsc,changes,buildinfo}{,.asc} rw,


  /{usr/,}bin/gpg rCx -> gpg,
  profile gpg {
    include <abstractions/base>

    /{usr/,}bin/gpg mr,

    owner @{HOME}/.gnupg/ r,
    owner @{HOME}/.gnupg/** rwkl -> @{HOME}/.gnupg/**,

    owner /tmp/debsign.*/*.{dsc,changes,buildinfo} r,
    owner /tmp/debsign.*/*.{dsc,changes,buildinfo}.asc rw,

  }

  include if exists <local/debsign>
}
