# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2024-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}lib/gvfs/gvfsd-wsdd
@{exec_path} += /usr/libexec/gvfsd-wsdd
profile gvfsd-wsdd @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  network netlink raw,
  network inet dgram,
  network inet6 dgram,

  @{exec_path} mr,

  /{usr/,}bin/wsdd rPx,

  owner @{run}/user/@{uid}/gvfsd/socket-* rw,

  include if exists <local/gvfsd-wsdd>
}
