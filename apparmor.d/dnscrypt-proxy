# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2018-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/dnscrypt-proxy
profile dnscrypt-proxy @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>
  include <abstractions/ssl_certs>

  # To bind to the 53 tcp/udp port (when systemd's sockets aren't used).
  capability net_bind_service,

  # Needed for privilege drop (to run as _dnscrypt-proxy:nogroup).
  capability setgid,
  capability setuid,

  # Needed?
  #capability net_admin,

  network inet dgram,
  network inet6 dgram,
  network inet stream,
  network inet6 stream,
  network netlink raw,

  @{exec_path} mrix,

  # dnscrypt-proxy config files
  @{etc_ro}/dnscrypt-proxy/ r,
  @{etc_ro}/dnscrypt-proxy/dnscrypt-proxy.toml r,
  @{etc_ro}/dnscrypt-proxy/allowed-names.txt r,
  @{etc_ro}/dnscrypt-proxy/blocked-names.txt r,
  @{etc_ro}/dnscrypt-proxy/cloaking-rules.txt r,
  @{etc_ro}/dnscrypt-proxy/forwarding-rules.txt r,

  # This is for the built-in DoH server / Firefox ESNI (Encrypted ClientHello)
  # See: https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Local-DoH
  owner /etc/dnscrypt-proxy/localhost.pem r,

  # For downloading the relays.md and public-resolvers.md files (for offline use, which can fix
  # connectivity issues).
  #owner /etc/dnscrypt-proxy/sf-*.tmp rw,
  #owner /etc/dnscrypt-proxy/relays.md rw,
  #owner /etc/dnscrypt-proxy/relays.md.minisig rw,
  #owner /etc/dnscrypt-proxy/public-resolvers.md rw,
  #owner /etc/dnscrypt-proxy/public-resolvers.md.minisig rw,
  #owner /etc/dnscrypt-proxy/odoh-relays.md rw,
  #owner /etc/dnscrypt-proxy/odoh-relays.md.minisig rw,
  #owner /etc/dnscrypt-proxy/odoh-servers.md rw,
  #owner /etc/dnscrypt-proxy/odoh-servers.md.minisig rw,

  /var/cache/dnscrypt-proxy/sf-*.tmp rw,
  /var/cache/dnscrypt-proxy/relays.md rw,
  /var/cache/dnscrypt-proxy/relays.md.minisig rw,
  /var/cache/dnscrypt-proxy/public-resolvers.md rw,
  /var/cache/dnscrypt-proxy/public-resolvers.md.minisig rw,
  /var/cache/dnscrypt-proxy/odoh-relays.md rw,
  /var/cache/dnscrypt-proxy/odoh-relays.md.minisig rw,
  /var/cache/dnscrypt-proxy/odoh-servers.md rw,
  /var/cache/dnscrypt-proxy/odoh-servers.md.minisig rw,

  /var/cache/private/dnscrypt-proxy/sf-*.tmp rw,
  /var/cache/private/dnscrypt-proxy/relays.md rw,
  /var/cache/private/dnscrypt-proxy/relays.md.minisig rw,
  /var/cache/private/dnscrypt-proxy/public-resolvers.md rw,
  /var/cache/private/dnscrypt-proxy/public-resolvers.md.minisig rw,
  /var/cache/private/dnscrypt-proxy/odoh-relays.md rw,
  /var/cache/private/dnscrypt-proxy/odoh-relays.md.minisig rw,
  /var/cache/private/dnscrypt-proxy/odoh-servers.md rw,
  /var/cache/private/dnscrypt-proxy/odoh-servers.md.minisig rw,

  @{etc_ro}/inittab r,
  @{PROC}/1/comm r,

  # Logs
  /var/log/dnscrypt-proxy/ r,
  /var/log/dnscrypt-proxy/*.log w,
  /var/log/private/dnscrypt-proxy/ rw,
  /var/log/private/dnscrypt-proxy/*.log w,

  @{PROC}/sys/net/core/somaxconn r,
  @{PROC}/sys/kernel/hostname r,

  # Needed?
  deny /etc/ssl/certs/java/ r,

  include if exists <local/dnscrypt-proxy>
}
