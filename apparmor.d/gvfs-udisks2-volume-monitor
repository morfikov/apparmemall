# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}lib/gvfs/gvfs-udisks2-volume-monitor
@{exec_path} += /usr/libexec/gvfs-udisks2-volume-monitor
profile gvfs-udisks2-volume-monitor  @{exec_path} flags=(attach_disconnected) {
  include <abstractions/base>
  include <abstractions/dconf>
  include <abstractions/freedesktop.org>
  include <abstractions/nameservice-strict>
  include <abstractions/disks-read>
  include <abstractions/devices-usb>

  network inet stream,
  network inet6 stream,
  network netlink raw,

  signal (send) set=(term, kill),

  ptrace (read),

  capability sys_ptrace,

  @{exec_path} mr,

  /{usr/,}bin/lsof   rix,

  /{usr/,}bin/mount  rPUx,
  /{usr/,}bin/umount rPUx,

  @{run}/systemd/sessions/[0-9]* r,

  @{etc_ro}/fstab r,

  # Mount points
  /media/*/ r,
  /media/*/*/ r,
  @{HOME}/*/*/ r,
  @{HOME}/*/*/**/ r,
  @{HOME}/bluetooth/ r,

  / r,

  @{run}/mount/utab r,

  /dev/dri/card[0-9] r,
  /dev/input/event[0-9]* r,

        @{PROC}/ r,
  owner @{PROC}/@{pid}/fd/ r,
  owner @{PROC}/@{pid}/mountinfo r,
  owner @{PROC}/@{pid}/mounts r,
  owner @{PROC}/@{pid}/fdinfo/* r,
        @{PROC}/@{pids}/attr/current r,
        @{PROC}/@{pids}/stat r,
        @{PROC}/@{pid}/net/netlink r,
        @{PROC}/@{pid}/net/packet r,
        @{PROC}/@{pid}/net/unix r,
        @{PROC}/@{pid}/net/raw{,6} r,
        @{PROC}/@{pid}/net/tcp{,6} r,
        @{PROC}/@{pid}/net/udp{,6} r,
        @{PROC}/@{pid}/net/udplite{,6} r,
        @{PROC}/@{pid}/net/icmp{,6} r,
        @{PROC}/@{pid}/net/sockstat{,6} r,
        @{PROC}/locks r,

  include if exists <local/gvfs-udisks2-volume-monitor>
}
