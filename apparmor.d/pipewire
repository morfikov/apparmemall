# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}bin/pipewire
@{exec_path} += /{usr/,}bin/pipewire-pulse
profile pipewire @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  ptrace (read) peer=pipewire-media-session,

  # Needed for all sound/music apps.
  ptrace (read),

  @{exec_path} mr,

  # info="no new privs"
  /{usr/,}bin/pactl rix,
  /var/lib/dbus/machine-id r,
  @{etc_ro}/machine-id r,

  @{etc_ro}/pipewire/*.conf r,
  @{etc_ro}/pipewire/media-session.d/*.conf r,
  /usr/share/pipewire/*.conf r,
  /usr/share/pipewire/media-session.d/*.conf r,

  owner @{PROC}/@{pid}/task/@{tid}/comm rw,

  /dev/snd/controlC[0-9]* rw,
  /dev/snd/pcmC[0-9]*D[0-9]*p rw,
  /dev/snd/pcmC[0-9]*D[0-9]*c rw,
  /dev/snd/seq rw,

  /usr/share/alsa/{,**} r,
  @{etc_ro}/alsa/{,**} r,

  /dev/shm/ r,
  @{run}/shm/ r,
  @{etc_ro}/pulse/{,**} r,
  owner @{HOME}/.config/pulse/ rw,
  owner @{HOME}/.config/pulse/cookie rwk,
  owner @{run}/user/@{uid}/pulse/ rw,
  owner @{run}/user/@{pid}/pulse/pid w,

  owner @{run}/user/@{uid}/pipewire-[0-9]*.lock rwk,
  owner @{run}/user/@{uid}/pulse/native-pipewire-[0-9]* rw,

  @{sys}/devices/virtual/dmi/id/product_name r,
  @{sys}/devices/virtual/dmi/id/sys_vendor r,
  @{sys}/devices/virtual/dmi/id/board_vendor r,
  @{sys}/devices/virtual/dmi/id/bios_vendor r,

  # For determining the following
  #  pipewire-pulse[]: default: snap_get_audio_permissions: kernel lacks 'fine grained unix
  #  mediation'; snap audio permissions won't be honored.
  @{sys}/module/apparmor/parameters/enabled r,

  / r,

  owner @{HOME}/.Xauthority r,

  owner @{HOME}/.cache/event-sound-cache.* rwk,

  /usr/share/sounds/freedesktop/index.theme r,

  include if exists <local/pipewire>
}
