# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2015-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/bluetoothctl
profile bluetoothctl @{exec_path} {
  include <abstractions/base>

  network bluetooth raw,

  @{exec_path} mr,

  @{etc_ro}/inputrc r,

  /usr/share/terminfo/** r,

  owner @{HOME}/.cache/ rw,
  owner @{HOME}/.cache/.bluetoothctl_history rw,
  owner @{HOME}/.cache/.bluetoothctl_history-@{pid}.tmp rw,

  include if exists <local/bluetoothctl>
}
