# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2018-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/dhclient
profile dhclient @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>
  include <abstractions/openssl>

  # To remove the following errors:
  #  dhclient[]: Open a socket for LPF: Operation not permitted
  capability net_raw,

  # To remove the following errors:
  #  dhclient[]: Can't bind to dhcp address: Permission denied
  capability net_bind_service,

  # Needed?
  audit deny capability net_admin,
  audit deny capability sys_module,

  network inet dgram,
  network inet6 dgram,
  network netlink raw,
  network packet raw,

  @{exec_path} mr,

  # To run dhclient scripts
  /{usr/,}sbin/dhclient-script rPx,

  @{etc_ro}/dhclient.conf r,
  @{etc_ro}/dhcp/{,**} r,

  /var/lib/dhcp{,3}/dhclient* rw,
  owner @{run}/dhclient*.pid rw,
  owner @{run}/dhclient*.lease* rw,

  owner @{PROC}/@{pid}/task/@{tid}/comm rw,

  include if exists <local/dhclient>
}
