# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2020-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/obconf
profile obconf @{exec_path} {
  include <abstractions/base>
  include <abstractions/gtk>
  include <abstractions/dconf>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/freedesktop.org>
  include <abstractions/nameservice-strict>
  include <abstractions/user-download-strict>

  @{exec_path} mr,

  /usr/share/obconf/{,*} r,

  @{etc_ro}/xdg/openbox/rc.xml r,

  owner @{HOME}/.config/openbox/rc.xml rw,

  owner @{HOME}/.themes/{,**} r,

  owner @{PROC}/@{pid}/mountinfo r,
  owner @{PROC}/@{pid}/mounts r,

  @{etc_ro}/fstab r,

  # file_inherit
  owner /dev/tty[0-9]* rw,

  include if exists <local/obconf>
}
