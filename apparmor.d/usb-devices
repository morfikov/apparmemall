# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/usb-devices
profile usb-devices @{exec_path} {
  include <abstractions/base>
  include <abstractions/devices-usb>

  @{exec_path} r,
  /{usr/,}bin/{,ba,da}sh rix,

  /{usr/,}bin/cat        rix,
  /{usr/,}bin/cut        rix,
  /{usr/,}bin/{,e}grep   rix,
  /{usr/,}bin/basename   rix,
  /{usr/,}bin/readlink   rix,

  # For shell pwd
  /root/ r,

  include if exists <local/usb-devices>
}
