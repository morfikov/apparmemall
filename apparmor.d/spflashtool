# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2020-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /opt/SPFlashTool/flash_tool{,.sh}
profile spflashtool @{exec_path} {
  include <abstractions/base>
  include <abstractions/X>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/freedesktop.org>

  @{exec_path} mrix,

  # SPFlashTool installation files
  /opt/SPFlashTool/{,**} r,
  /opt/SPFlashTool/lib*.so mr,
  /opt/SPFlashTool/lib/lib*.so.[0-9]* mr,
  /opt/SPFlashTool/*.ini rk,

  # Session logs
  owner /tmp/SP_FT_Logs/ rw,
  owner /tmp/SP_FT_Logs/SP_FT_Dump_*/ rw,
  owner /tmp/SP_FT_Logs/SP_FT_Dump_*1/QT_FLASH_TOOL.log w,
  owner /tmp/SP_FT_Logs/SP_FT_Dump_*/BROM_DLL_V[0-9]*.log w,
  owner /tmp/SP_FT_Logs/SP_FT_Dump_*/GLB_[0-9]*-[0-9]*_[0-9]*.log w,
  owner /tmp/SP_FT_Logs/SP_FT_Dump_*/QT_FLASH_TOOL.log w,
  owner /tmp/SP_FT_Logs/SP_FT_Dump_*/ADPT_[0-9]*-[0-9]*_[0-9]*.log w,

  # For reading the scatter.txt file
  owner /**/scatter.txt r,

  owner @{HOME}/.config/Trolltech.conf rwk,

  owner @{HOME}/.config/MTK/ rw,
  owner @{HOME}/.config/MTK/Clipper.conf rwk,

  /dev/ r,
  # For reading/writing from/to phone flash memory
  /dev/ttyACM[0-9]* rw,

  @{sys}/devices/pci[0-9]*/**/{idVendor,idProduct} r,

  # Silence the noise
  /opt/SPFlashTool/** w,

  include if exists <local/spflashtool>
}
