# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/ganyremote
profile ganyremote @{exec_path} {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/nameservice-strict>
  include <abstractions/gtk>
  include <abstractions/freedesktop.org>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/user-download-strict>
  include <abstractions/python>
  include <abstractions/thumbnails-cache-read>
  include <abstractions/openssl>

  network inet stream,
  network inet6 stream,

  @{exec_path} r,

  /{usr/,}bin/{,ba,da}sh  rix,
  /{usr/,}bin/rm          rix,
  /{usr/,}bin/{,e}grep    rix,
  /{usr/,}bin/cut         rix,
  /{usr/,}bin/id          rix,
  /{usr/,}bin/which{,.debianutils}       rix,
  /{usr/,}bin/tr          rix,
  /{usr/,}bin/gawk        rix,

  /{usr/,}bin/anyremote   rPx,
  /{usr/,}bin/ps          rPx,

  /{usr/,}bin/killall     rCx -> killall,
  /{usr/,}bin/pgrep       rCx -> pgrep,

  /{usr/,}bin/pacmd      rPUx,
  /{usr/,}bin/pactl      rPUx,

  # Players
  /{usr/,}bin/smplayer   rPUx,
  /{usr/,}bin/amarok     rPUx,
  /{usr/,}bin/vlc        rPUx,
  /{usr/,}bin/mpv        rPUx,
  /{usr/,}bin/strawberry rPUx,

  owner @{HOME}/ r,
  owner @{HOME}/.anyRemote/{,*} rw,

  /usr/share/anyremote/{,**} r,

  deny       @{PROC}/sys/kernel/osrelease r,
       owner @{PROC}/@{pid}/mounts r,
       owner @{PROC}/@{pid}/mountinfo r,

  @{etc_ro}/fstab r,

  # Doc dirs
  deny /usr/local/share/ r,
  deny /usr/share/ r,
  deny /usr/share/doc/ r,
       /usr/share/doc/anyremote{,-data}/ r,


  profile killall {
    include <abstractions/base>
    include <abstractions/consoles>

    capability sys_ptrace,

    signal (send) set=(int, term, kill),

    ptrace (read),

    /{usr/,}bin/killall mr,

    # The /proc/ dir is needed to avoid the following error:
    #  /proc: Permission denied
         @{PROC}/ r,
         @{PROC}/@{pids}/ r,
         @{PROC}/@{pids}/stat r,
         @{PROC}/@{pids}/cmdline r,

  }

  profile pgrep {
    include <abstractions/base>
    include <abstractions/consoles>

    capability sys_ptrace,

    ptrace (read),

    /{usr/,}bin/pgrep mr,

    # The /proc/ dir and the cmdline file have to be radable to avoid pgrep segfault.
	@{PROC}/ r,
	@{PROC}/@{pids}/cmdline r,
	@{PROC}/@{pids}/stat r,
	@{PROC}//uptime r,
	@{PROC}/sys/kernel/osrelease r,
    @{PROC}/tty/drivers r,

	@{sys}/devices/system/node/ r,
    @{sys}/devices/system/node/node[0-9]*/meminfo r,

    /usr/share/anyremote/{,**} r,

  }

  include if exists <local/ganyremote>
}
