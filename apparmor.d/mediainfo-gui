# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

# Video/audio extensions:
# a52, aac, ac3, mka, flac, mp1, mp2, mp3, mpc, oga, oma, wav, wv, wm, wma, 3g2, 3gp, 3gp2, 3gpp,
# asf, avi, divx, m1v, m2v, m4v, mkv, mov, mp4, mpa, mpe, mpg, mpeg, mpeg1, mpeg2, mpeg4, ogg, ogm,
# ogx, ogv, rm, rmvb, webm, wmv, wtv, mp2t
@{mediainfo_ext}  = [aA]{52,[aA][cC],[cC]3}
@{mediainfo_ext} += [mM][kK][aA]
@{mediainfo_ext} += [fF][lL][aA][cC]
@{mediainfo_ext} += [mM][pP][123cC]
@{mediainfo_ext} += [oO][gGmM][aA]
@{mediainfo_ext} += [wW]{,[aA]}[vV]
@{mediainfo_ext} += [wW][mM]{,[aA]}
@{mediainfo_ext} += 3[gG]{[2pP],[pP][2pP]}
@{mediainfo_ext} += [aA][sS][fF]
@{mediainfo_ext} += [aA][vV][iI]
@{mediainfo_ext} += [dD][iI][vV][xX]
@{mediainfo_ext} += [mM][124][vV]
@{mediainfo_ext} += [mM][kKoO][vV]
@{mediainfo_ext} += [mM][pP][4aAeEgG]
@{mediainfo_ext} += [mM][pP][eE][gG]{,[124]}
@{mediainfo_ext} += [oO][gG][gGmMxXvV]
@{mediainfo_ext} += [rR][mM]{,[vV][bB]}
@{mediainfo_ext} += [wW][eE][bB][mM]
@{mediainfo_ext} += [wW][mMtT][vV]
@{mediainfo_ext} += [mM][pP]2[tT]

@{exec_path} = /{usr/,}bin/mediainfo-gui
profile mediainfo-gui @{exec_path} {
  include <abstractions/base>
  include <abstractions/gtk>
  include <abstractions/dconf>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/freedesktop.org>
  include <abstractions/user-download-strict>
  include <abstractions/private-files-strict>

  @{exec_path} mr,

  /{usr/,}bin/xdg-open                                    rCx -> open,
  /{usr/,}bin/exo-open                                    rCx -> open,
  /{usr/,}lib/@{multiarch}/glib-[0-9]*/gio-launch-desktop rCx -> open,

  # Which media files mediainfo-gui should be able to open
        / r,
        /home/ r,
  owner @{HOME}/ r,
  owner @{HOME}/**/ r,
        /media/ r,
  owner /media/**/ r,
  owner /{home,media}/**.@{mediainfo_ext} r,

  owner @{HOME}/.mediainfo-gui* rw,


  profile open {
    include <abstractions/base>
    include <abstractions/xdg-open>

    /{usr/,}bin/xdg-open                                    mr,
    /{usr/,}bin/exo-open                                    mr,
    /{usr/,}lib/@{multiarch}/glib-[0-9]*/gio-launch-desktop mr,

    /{usr/,}bin/{,ba,da}sh      rix,
    /{usr/,}bin/gawk            rix,
    /{usr/,}bin/readlink        rix,
    /{usr/,}bin/basename        rix,
    /{usr/,}bin/realpath        rix,

    owner @{HOME}/ r,

    owner @{run}/user/@{uid}/ r,

    # Allowed apps to open
    /{usr/,}lib/firefox/firefox rPUx,

    # file_inherit
    owner @{HOME}/.xsession-errors w,

  }

  include if exists <local/mediainfo-gui>
}
