# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/cron
profile cron @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>
  include <abstractions/authentication>
  include <abstractions/wutmp>

       capability setuid,
       capability setgid,
       capability dac_read_search,
       capability audit_write,
       capability sys_resource,
  deny capability net_admin,

  network netlink raw,

  @{exec_path} mr,

  /{usr/,}bin/{,ba,da}sh      rix,
  /{usr/,}bin/nice            rix,
  /{usr/,}bin/ionice          rix,
  /{usr/,}bin/systemd-inhibit rix,
  /{usr/,}bin/schedtool       rix,

  @{etc_ro}/crontab r,

  # All stuff that is executed via the /etc/cron.d/ dir
  @{etc_ro}/cron.d/{,*} r,
  /{usr/,}sbin/cron-apt                 rPx,
  /{usr/,}bin/debsecan                  rPx,
  /{usr/,}lib/@{multiarch}/e2fsprogs/e2scrub_all_cron rPUx,
  /{usr/,}sbin/e2scrub_all             rPUx,
  @{etc_ro}/cron.daily/popularity-contest    rPx,
  /{usr/,}lib/sysstat/debian-sa1       rPUx,

  # All stuff that is executed via the user crontab files
  /{usr/,}bin/apt-file                  rPx,
  /{usr/,}bin/apt-key                   rPx,
  /{usr/,}bin/rsync                    rPUx,
  /usr/share/rsync/scripts/rrsync      rPUx,
  /{usr/,}bin/gpg                       rPx,
  /{usr/,}sbin/update-pciids            rPx,
  /{usr/,}bin/borg                      rPx,

  # Cron scripts in the /etc/cron.*/ dir to execute
  /{usr/,}bin/run-parts                 rCx -> run-parts,

  # Send results using email
  /{usr/,}sbin/exim4                    rPx,

  /var/spool/cron/crontabs/{,*} r,

  owner @{run}/crond.pid rwk,
  owner @{run}/crond.reboot rw,

  owner /tmp/#[0-9]*[0-9] rw,

  owner @{PROC}/@{pid}/uid_map r,
  owner @{PROC}/@{pid}/loginuid rw,

  @{etc_ro}/locale.conf r,
  @{etc_ro}/default/locale r,

  @{PROC}/1/limits r,


  profile run-parts {
    include <abstractions/base>

    /{usr/,}bin/run-parts mr,

    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/                     r,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/apt-listbugs         rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/apt-show-versions    rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/bsdmainutils        rPUx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/checksecurity       rPUx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/debtags              rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/exim4-base           rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/logrotate            rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/mlocate              rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/dlocate              rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/plocate              rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/passwd              rPUx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/apt-compat           rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/aptitude             rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/debsums              rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/dpkg                rPUx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/man-db               rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/popularity-contest   rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/sysstat              rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/spamassassin        rPUx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/vrms                rPUx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/apt-xapian-index     rPx,
    @{etc_ro}/cron.{hourly,daily,weekly,monthly}/tor                 rPUx,

    #@{etc_ro}/cron.{hourly,daily,weekly,monthly}/opera-browser                     rPUx,
    #@{etc_ro}/cron.{hourly,daily,weekly,monthly}/google-chrome{,-beta,-unstable}   rPUx,
    #/opt/google/chrome{,-beta,-unstable}/cron/google-chrome{,-beta,-unstable} rPUx,
    #/opt/brave.com/brave/cron/brave-browser{,-beta,-dev}                      rPUx,
    #/opt/brave.com/brave{,-beta,-dev}/cron/brave-browser{,-beta,-dev}         rPUx,

    # file_inherit
    owner /tmp/#[0-9]*[0-9] rw,

    include if exists <local/cron_run-parts>
  }

  include if exists <local/cron>
}
