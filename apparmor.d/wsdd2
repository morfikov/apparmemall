# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2024-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}sbin/wsdd2
profile wsdd2 @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  network netlink raw,
  network inet dgram,
  network inet6 dgram,
  network inet stream,
  network inet6 stream,

  @{exec_path} mr,
  /{usr/,}bin/{,ba,da}sh rix,
  /usr/bin/testparm rix,

  @{etc_ro}/samba/smb.conf r,

  /var/lib/dbus/machine-id r,
  @{etc_ro}/machine-id r,

  include if exists <local/wsdd2>
}
