# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/add{user,group}
profile adduser @{exec_path} flags=(complain) {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/perl>
  include <abstractions/nameservice-strict>

  # To create a user home dir and give it proper permissions:
  #  mkdir("/home/user", 0755)     = 0
  #  chown("/home/user", 1001, 1001) = 0
  #  chmod("/home/user", 0755)     = 0
  capability chown,
  capability fowner,

  # To set the set-group-ID bit for the user home dir (SETGID_HOME=yes).
  capability fsetid,

  # To copy files from the /etc/skel/ dir to the newly created user dir, which now has a different
  # owner.
  capability dac_read_search,
  capability dac_override,

  # For logger
  capability sys_admin,

  @{exec_path} r,
  /{usr/,}bin/perl r,

  /{usr/,}bin/{,ba,da}sh rix,
  /{usr/,}bin/find       rix,
  /{usr/,}bin/rm         rix,

  /{usr/,}sbin/useradd   rPx,
  /{usr/,}sbin/userdel   rPx,
  /{usr/,}sbin/groupdel  rPx,
  /{usr/,}sbin/groupadd  rPx,
  /{usr/,}sbin/usermod   rPx,
  /{usr/,}bin/passwd     rPx,
  /{usr/,}bin/gpasswd    rPx,
  /{usr/,}bin/chfn       rPx,
  /{usr/,}bin/chage      rPx,

  /{usr/,}bin/logger     rix,

  @{etc_ro}/{group,passwd,shadow} r,

  @{etc_ro}/adduser.conf r,

  owner @{run}/adduser rwk,

  # To create user dirs and copy files from /etc/skel/ to them
  @{HOME}/ rw,
  @{HOME}/.* w,
  /var/lib/*/{,*} rw,
  @{etc_ro}/skel/{,.*} r,

  include if exists <local/adduser>
}
