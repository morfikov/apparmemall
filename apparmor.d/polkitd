# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2018-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}lib/polkit-1/polkitd
profile polkitd @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  # Tu run as polkitd:nogroup
  capability setuid,
  capability setgid,

  # Needed?
  audit deny capability net_admin,

  ptrace (read),

  @{exec_path} mr,

        @{PROC}/@{pids}/stat r,
        @{PROC}/@{pids}/cmdline r,
  owner @{PROC}/@{pid}/task/@{tid}/stat r,
  owner @{PROC}/@{pid}/fdinfo/* r,
        @{PROC}/sys/kernel/osrelease r,
        @{PROC}/1/environ r,
        @{PROC}/cmdline r,

  # System rules
  @{etc_ro}/polkit-1/rules.d/ r,
  @{etc_ro}/polkit-1/rules.d/[0-9][0-9]-*.rules r,

  # Vendor rules
  /usr/share/polkit-1/rules.d/ r,
  /usr/share/polkit-1/rules.d/*.rules r,

  # Vendor policies
  /usr/share/polkit-1/actions/ r,
  /usr/share/polkit-1/actions/*.policy r,
  /usr/share/polkit-1/actions/*.policy.choice r,

  owner /var/lib/polkit-1/.cache/ rw,

  /usr/share/gvfs/remote-volume-monitors/ r,
  /usr/share/gvfs/remote-volume-monitors/*.monitor r,

  @{run}/systemd/sessions/* r,
  @{run}/systemd/users/[0-9]* r,

  include if exists <local/polkitd>
}
