# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

# Note: This profile does not specify an attachment path because it is
# intended to be used only via "Px -> child-lsb_release" exec transitions
# from other profiles. We want to confine the lsb_release(1) utility when
# it is invoked from other confined applications, but not when it is used
# in regular (unconfined) shell scripts or run directly by the user.

abi <abi/4.0>,

include <tunables/global>

# Do not attach to /{usr/,}bin/lsb_release by default
profile child-lsb_release {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/python>

  signal (receive) set=(term, kill),

  owner @{PROC}/@{pid}/fd/ r,

  /{usr/,}bin/lsb_release r,

  @{etc_ro}/debian_version r,
# @{etc_ro}/default/apport r,
  @{etc_ro}/dpkg/origins/** r,
# @{etc_ro}/lsb-release r,
# @{etc_ro}/lsb-release.d/ r,

  /{usr/,}bin/{,ba,da}sh rix,
# /{usr/,}bin/basename ixr,

  /{usr/,}bin/getopt ixr,
# /{usr/,}bin/sed ixr,
  /{usr/,}bin/tr ixr,
  /{usr/,}bin/cut ixr,

  # Do not strip env to avoid errors like the following:
  #  ERROR: ld.so: object 'libfakeroot-sysv.so' from LD_PRELOAD cannot be preloaded (cannot open
  #  shared object file): ignored.
  /{usr/,}bin/dpkg-query rix,

  /{usr/,}bin/apt-cache  rPx,

# /usr/include/python*/pyconfig.h r,
  /usr/share/distro-info/*.csv r,
# /usr/share/dpkg/** r,
# /usr/share/terminfo/** r,
# /var/lib/dpkg/** r,

  # file_inherit
  owner /dev/tty[0-9]* rw,
  owner @{HOME}/.xsession-errors w,
# deny /tmp/gtalkplugin.log w,
  /dev/dri/card[0-9]* rw,

  # Silencer
  deny /usr/lib/python3/** w,

  # Site-specific additions and overrides. See local/README for details.
  include if exists <local/child-lsb_release>
}
