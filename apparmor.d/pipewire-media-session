# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/pipewire-media-session
profile pipewire-media-session @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  network netlink raw,

  @{exec_path} mr,

  @{etc_ro}/pipewire/*.conf r,
  @{etc_ro}/pipewire/media-session.d/*.conf r,
  /usr/share/pipewire/*.conf r,
  /usr/share/pipewire/media-session.d/*.conf r,

  owner @{HOME}/.config/pipewire/ rw,
  owner @{HOME}/.config/pipewire/** rw,

  /dev/snd/controlC[0-9]* rw,
  /dev/snd/pcmC[0-9]*D[0-9]*p rw,
  /dev/snd/pcmC[0-9]*D[0-9]*c rw,

  /usr/share/alsa-card-profile/{,**} r,
  /usr/share/alsa/{,**} r,
  @{etc_ro}/alsa/{,**} r,

  /dev/shm/ r,
  @{run}/shm/ r,
  @{etc_ro}/pulse/{,**} r,
  owner @{HOME}/.config/pulse/ rw,
  owner @{HOME}/.config/pulse/cookie rwk,
  owner @{run}/user/@{uid}/pulse/ rw,

  @{sys}/bus/ r,
  @{sys}/class/ r,
  @{sys}/class/sound/ r,
  @{sys}/class/video4linux/ r,

  @{sys}/devices/**/sound/**/uevent r,

  @{run}/udev/data/+sound:card[0-9]* r, # For sound
  @{run}/udev/data/c116:[0-9]* r,       # For ALSA

  @{run}/systemd/users/@{uid} r,

  @{sys}/devices/system/node/ r,
  @{sys}/devices/system/node/node[0-9]*/meminfo r,

  include if exists <local/pipewire-media-session>
}
