# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/ifconfig
profile ifconfig @{exec_path} {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/nameservice-strict>

  # To be able to manage network interfaces.
  capability net_admin,

  # Needed?
  audit deny capability sys_module,

  network inet dgram,
  network inet6 dgram,

  @{exec_path} mr,

  @{PROC}/net/dev r,
  @{PROC}/net/if_inet6 r,
  @{PROC}/@{pid}/net/dev r,
  @{PROC}/@{pid}/net/if_inet6 r,

  @{etc_ro}/networks r,

  include if exists <local/ifconfig>
}
