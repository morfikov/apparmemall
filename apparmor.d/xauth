# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2020-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/xauth
profile xauth @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  @{exec_path} mr,

  owner @{HOME}/.Xauthority-c  w,
  owner @{HOME}/.Xauthority-l  wl -> @{HOME}/.Xauthority-c,
  owner @{HOME}/.Xauthority-n rw,
  owner @{HOME}/.Xauthority   rwl -> @{HOME}/.Xauthority-n,

  owner /tmp/serverauth.*-c  w,
  owner /tmp/serverauth.*-l  wl -> /tmp/serverauth.*-c,
  owner /tmp/serverauth.*-n rw,
  owner /tmp/serverauth.*   rwl -> /tmp/serverauth.*-n,

  include if exists <local/xauth>
}
