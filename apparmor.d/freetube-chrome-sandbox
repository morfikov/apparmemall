# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{FT_LIBDIR} =  /{usr/,}lib/freetube
@{FT_LIBDIR} += /{usr/,}lib/freetube-vue
@{FT_LIBDIR} += /opt/FreeTube
@{FT_LIBDIR} += /opt/FreeTube-Vue

@{exec_path} = @{FT_LIBDIR}/chrome-sandbox
profile freetube-chrome-sandbox @{exec_path} {
  include <abstractions/base>
  include <abstractions/consoles>

  capability sys_admin,
  capability setgid,
  capability setuid,
  capability sys_chroot,

  @{exec_path} mr,

  # Has to be lower "P"
  @{FT_LIBDIR}/freetube{,-vue} rpx,

             @{PROC}/@{pids}/ r,
       owner @{PROC}/@{pid}/oom_{,score_}adj r,
  deny owner @{PROC}/@{pid}/oom_{,score_}adj w,

  include if exists <local/freetube-chrome-sandbox>
}
