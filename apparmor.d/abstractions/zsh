# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2018-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

  abi <abi/4.0>,

  /usr/share/zsh/{,**} r,
  /usr/local/share/zsh/{,**} r,

  /{usr/,}lib/@{multiarch}/zsh/[0-9]*/zsh/*.so mr,

  @{etc_ro}/zsh/zshenv r,
  @{etc_ro}/zsh/zshrc r,
  @{etc_ro}/zsh/zprofile r,
  @{etc_ro}/zsh/zlogin r,

  owner @{HOME}/.zshrc r,
  owner @{HOME}/.zsh_history rw,
  owner @{HOME}/.zsh_history.LOCK rwk,

  owner @{HOME}/.oh-my-zsh/{,**} r,
  owner @{HOME}/.oh-my-zsh/log/update.lock/ w,

  owner @{HOME}/.zcompdump-* rw,
