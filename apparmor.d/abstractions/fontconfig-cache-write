# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2018-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

  abi <abi/4.0>,

  owner @{HOME}/.cache/fontconfig/ rw,
  owner @{HOME}/.cache/fontconfig/CACHEDIR.TAG{,.NEW,.LCK,.TMP-*} rw,
  owner @{HOME}/.cache/fontconfig/[a-f0-9]*.cache-?{,.NEW,.LCK,.TMP-*} rwk,

  owner @{HOME}/.fontconfig/ rw,
  owner @{HOME}/.fontconfig/CACHEDIR.TAG{,.NEW,.LCK,.TMP-*} rw,
  owner @{HOME}/.fontconfig/[a-f0-9]*.cache-?{,.NEW,.LCK,.TMP-*} rwk,

  owner @{HOME}/.fonts/ rw,
   link @{HOME}/.fonts/.uuid.LCK -> @{HOME}/.fonts/.uuid.TMP-*,
  owner @{HOME}/.fonts/.uuid{,.NEW,.LCK,.TMP-*}  r,
  owner @{HOME}/.fonts/.uuid{,.NEW,.LCK,.TMP-*}  w,

  # This is to create .uuid file containing an UUID at a font directory. The UUID will be used to
  # identify the font directory and is used to determine the cache filename if available.
  owner /usr/local/share/fonts/ rw,
  owner /usr/local/share/fonts/.uuid{,.NEW,.LCK,.TMP-*} rw,
   link /usr/local/share/fonts/.uuid.LCK -> /usr/local/share/fonts/.uuid.TMP-*,
  # Should writing to these dirs be blocked?
        /usr/share/**/.uuid{,.NEW,.LCK,.TMP-*}  r,
  deny  /usr/share/**/.uuid{,.NEW,.LCK,.TMP-*}  w,

  # For fonts downloaded via font-manager (###FIXME### when they fix resolving of vars)
  owner @{HOME}/.local/share/fonts/ rw,
  owner @{HOME}/.local/share/fonts/**/.uuid{,.NEW,.LCK,.TMP-*} rw,
   link @{HOME}/.local/share/fonts/**/.uuid.LCK -> /home/*/.local/share/fonts/**/.uuid.TMP-*,
