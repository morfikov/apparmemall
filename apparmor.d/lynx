# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/lynx
profile lynx @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>
  include <abstractions/wutmp>
  include <abstractions/openssl>
  include <abstractions/ssl_certs>

  network inet dgram,
  network inet6 dgram,
  network inet stream,
  network inet6 stream,

  @{exec_path} mr,

  @{etc_ro}/lynx/{,*} r,

  /usr/share/doc/lynx-common/** r,

  @{etc_ro}/mime.types r,

  /{usr/,}bin/{,ba,da}sh rix,
  @{etc_ro}/mailcap r,

  owner /tmp/lynxXXXX*/ rw,
  owner /tmp/lynxXXXX*/*TMP.html{,.gz} rw,

  owner @{HOME}/ r,

  include if exists <local/lynx>
}
