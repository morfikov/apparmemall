# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/iotop
profile iotop @{exec_path} {
  include <abstractions/base>
  include <abstractions/python>
  include <abstractions/nameservice-strict>

  # Needed?
  audit deny capability net_admin,

  # To set processes' priorities
  capability sys_nice,

  @{exec_path} r,

  /{usr/,}bin/file rix,

  /{usr/,}sbin/ r,

        @{PROC}/ r,
        @{PROC}/vmstat r,
  owner @{PROC}/@{pid}/mounts r,
  owner @{PROC}/@{pid}/fd/ r,
        @{PROC}/@{pids}/cmdline r,
        @{PROC}/@{pids}/task/ r,
        @{PROC}/sys/kernel/pid_max r,

  # For file
  @{etc_ro}/magic r,

  include if exists <local/iotop>
}
