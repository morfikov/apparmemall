# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2018-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/exo-open
profile exo-open @{exec_path} {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/gtk>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/freedesktop.org>
  include <abstractions/app-launcher-user>

  @{exec_path} mr,

  /{usr/,}lib/@{multiarch}/xfce4/exo-[0-9]/exo-helper-[0-9] rPx,

  # It looks like gio-launch-desktop decides what app should be opened
  /{usr/,}lib/@{multiarch}/glib-[0-9]*/gio-launch-desktop rPx,

  owner @{PROC}/@{pid}/fd/ r,

  /** r,
  #owner @{HOME}/** rw,
  #owner /media/*/** rw,

  include if exists <local/exo-open>
}
