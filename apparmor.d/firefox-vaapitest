# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2020-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{MOZ_LIBDIR} = /{usr/,}lib/firefox
@{MOZ_HOMEDIR} = @{HOME}/.mozilla
@{MOZ_CACHEDIR} = @{HOME}/.cache/mozilla

@{exec_path} = @{MOZ_LIBDIR}/vaapitest
profile firefox-vaapitest @{exec_path} {
  include <abstractions/base>
  include <abstractions/mesa>
  include <abstractions/dri-common>

  network netlink raw,

  @{exec_path} mr,

  owner /tmp/firefox/.parentlock rw,

  @{sys}/devices/pci[0-9]*/**/config r,

  # file_inherit
  owner @{HOME}/.xsession-errors w,
  owner @{HOME}/.mozilla/firefox/*/.parentlock rw,
  owner @{HOME}/.mozilla/firefox/*/extensions/*.xpi r,
  owner @{HOME}/.cache/mozilla/firefox/*/startupCache/scriptCache-* r,
  owner @{HOME}/.cache/mozilla/firefox/*/startupCache/startupCache* r,

  include if exists <local/firefox-vaapitest>
}
