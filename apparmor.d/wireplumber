# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/wireplumber
profile wireplumber @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>

  network netlink raw,
  network bluetooth seqpacket,
  network bluetooth stream,
  network bluetooth raw,

  @{exec_path} mr,

  @{etc_ro}/pipewire/*.conf r,
  @{etc_ro}/pipewire/media-session.d/*.conf r,
  /usr/share/pipewire/*.conf r,
  /usr/share/pipewire/media-session.d/*.conf r,

  @{etc_ro}/wireplumber/{,**} r,
  /usr/share/wireplumber/{,**} r,

  /usr/share/spa-*/bluez5/*.conf r,

  owner @{HOME}/.local/state/wireplumber/ rw,
  owner @{HOME}/.local/state/wireplumber/* rw,

  /dev/snd/ r,
  /dev/snd/controlC[0-9]* rw,
  /dev/snd/pcmC[0-9]*D[0-9]*p rw,
  /dev/snd/pcmC[0-9]*D[0-9]*c rw,

  /usr/share/alsa-card-profile/{,**} r,
  /usr/share/alsa/{,**} r,
  @{etc_ro}/alsa/{,**} r,

        @{run}/shm/ r,
        /dev/shm/ r,
  owner /dev/shm/lttng-ust-wait-8 rw,
  owner /dev/shm/lttng-ust-wait-8-@{uid} rw,

  @{etc_ro}/pulse/{,**} r,

  /var/lib/dbus/machine-id r,
  @{etc_ro}/machine-id r,

  owner @{HOME}/.config/pulse/ rw,
  owner @{HOME}/.config/pulse/cookie rwk,
  owner @{run}/user/@{uid}/pulse/ rw,

  @{sys}/bus/ r,
  @{sys}/bus/media/devices/ r,
  @{sys}/class/ r,
  @{sys}/class/sound/ r,
  @{sys}/class/video4linux/ r,

  @{sys}/devices/**/sound/**/uevent r,
  @{sys}/devices/pci[0-9]*/**/usb[1-4]/**/modalias r,

  @{run}/udev/data/+sound:card[0-9]* r, # For sound
  @{run}/udev/data/c116:[0-9]* r,       # For ALSA

  @{run}/systemd/users/@{uid} r,

  @{sys}/devices/system/node/ r,
  @{sys}/devices/system/node/node[0-9]*/meminfo r,

  @{sys}/devices/**/sound/card[0-9]*/**/pcm_class r,

  @{sys}/devices/virtual/dmi/id/sys_vendor r,
  @{sys}/devices/virtual/dmi/id/product_name r,
  @{sys}/devices/virtual/dmi/id/board_vendor r,
  @{sys}/devices/virtual/dmi/id/bios_vendor r,

  @{PROC}/asound/card[0-9]*/ r,
  @{PROC}/asound/card[0-9]*/pcm[0-9]*c/ r,
  @{PROC}/asound/card[0-9]*/pcm[0-9]*p/ r,
  @{PROC}/asound/card[0-9]*/pcm[0-9]*c/sub[0-9]*/status r,
  @{PROC}/asound/card[0-9]*/pcm[0-9]*p/sub[0-9]*/status r,

  # To fix the following:
  #  wireplumber[]: pthread_setname error: Permission denied
  owner @{PROC}/@{pid}/task/@{tid}/comm rw,

  include if exists <local/wireplumber>
}
