# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{BUILD_DIR}  = /media/debuilder/
@{BUILD_DIR} += /tmp/kernel-build-morfik/
@{BUILD_DIR} += /usr/src/

@{exec_path}  = /{usr/,}bin/apt-extracttemplates
@{exec_path} += /usr/lib/apt/apt-extracttemplates
profile apt-extracttemplates @{exec_path} {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/apt-common>

  @{exec_path} mr,

  /{usr/,}bin/dpkg rPUx,

  owner @{PROC}/@{pid}/fd/ r,

  /var/cache/apt/ r,
  /var/cache/apt/** rwk,

  owner /tmp/*.{config,template}.?????? rw,
  owner /var/cache/debconf/tmp.ci/*.{config,template}.?????? rw,

  # For package building
  @{BUILD_DIR}/** rwkl -> @{BUILD_DIR}/**,

  include if exists <local/apt-extracttemplates>
}
