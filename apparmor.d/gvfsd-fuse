# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}lib/gvfs/gvfsd-fuse
@{exec_path} += /usr/libexec/gvfsd-fuse
profile gvfsd-fuse @{exec_path} {
  include <abstractions/base>

  capability sys_admin,

  @{exec_path} mr,

  /{usr/,}bin/fusermount{,3} rCx -> fusermount,

  mount fstype={fuse,fuse.*} -> @{run}/user/@{uid}/gvfs/,

  umount @{run}/user/@{uid}/gvfs/,

  /dev/fuse rw,

  @{PROC}/sys/fs/pipe-max-size r,


  profile fusermount {
    include <abstractions/base>
    include <abstractions/nameservice-strict>

    # To mount anything:
    capability sys_admin,

    capability dac_read_search,

    /{usr/,}bin/fusermount{,3} mr,

    mount fstype={fuse,fuse.*} -> @{run}/user/@{uid}/gvfs/,
    umount @{run}/user/@{uid}/**/,

    @{etc_ro}/fuse.conf r,

    /dev/fuse rw,

    @{PROC}/@{pid}/mounts r,

  }

  include if exists <local/gvfsd-fuse>
}
