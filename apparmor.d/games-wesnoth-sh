# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /usr/games/wesnoth-[0-9]*{-nolog,-smalgui,_editor} /usr/games/wesnoth-nolog
profile games-wesnoth-sh @{exec_path} {
  include <abstractions/base>

  @{exec_path} r,
  /{usr/,}bin/{,ba,da}sh       rix,

  /usr/games/wesnoth{,-[0-9]*} rPx,

  # For the editor
  /{usr/,}bin/basename         rix,
  /{usr/,}bin/sed              rix,

  # file_inherit
  owner @{HOME}/.xsession-errors w,

  include if exists <local/games-wesnoth-sh>
}
