# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/groupdel
profile groupdel @{exec_path} flags=(complain) {
  include <abstractions/base>
  include <abstractions/consoles>
  include <abstractions/nameservice-strict>

  # To write records to the kernel auditing log.
  capability audit_write,

  # To set the right permission to the files in the /etc/ dir.
  capability chown,
  capability fsetid,

  network netlink raw,

  @{exec_path} mr,

  @{etc_ro}/login.defs r,

  @{etc_rw}/{group,gshadow} rw,
  @{etc_rw}/{group,gshadow}.@{pid} w,
  @{etc_rw}/{group,gshadow}- w,
  @{etc_rw}/{group,gshadow}+ rw,
  @{etc_rw}/group.lock wl -> @{etc_rw}/group.@{pid},
  @{etc_rw}/gshadow.lock wl -> @{etc_rw}/gshadow.@{pid},

  # A process first uses lckpwdf() to lock the lock file, thereby gaining exclusive rights to
  # modify the /etc/passwd or /etc/shadow password database.
  @{etc_rw}/.pwd.lock rwk,

  include if exists <local/groupdel>
}
