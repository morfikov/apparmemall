# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/amixer
profile amixer @{exec_path} {
  include <abstractions/base>
  include <abstractions/audio>
  include <abstractions/nameservice-strict>

  @{exec_path} mr,

  /var/lib/dbus/machine-id r,
  @{etc_ro}/machine-id r,

  owner @{HOME}/.Xauthority r,

  owner @{PROC}/@{pid}/task/@{tid}/comm rw,

  # file_inherit
  owner /dev/tty[0-9]* rw,

  include if exists <local/amixer>
}
