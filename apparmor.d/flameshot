# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/flameshot
profile flameshot @{exec_path} {
  include <abstractions/base>
  include <abstractions/X>
  include <abstractions/gtk>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/freedesktop.org>
  include <abstractions/dri-enumerate>
  include <abstractions/mesa>
  include <abstractions/qt-compose-cache-write>
  include <abstractions/qt-settings-write>
  include <abstractions/thumbnails-cache-read>
  include <abstractions/user-download-strict>
  include <abstractions/nameservice-strict>
  include <abstractions/openssl>
  include <abstractions/ssl_certs>

  network inet dgram,
  network inet6 dgram,
  network inet stream,
  network inet6 stream,
  network netlink raw,
  network netlink dgram,

  @{exec_path} mr,

  /{usr/,}bin/whoami     rix,

  /{usr/,}bin/xdg-open                                    rCx -> open,
  /{usr/,}bin/exo-open                                    rCx -> open,
  /{usr/,}lib/@{multiarch}/glib-[0-9]*/gio-launch-desktop rCx -> open,

  # Flameshot home files
  owner @{HOME}/.config/flameshot/ rw,
  owner @{HOME}/.config/flameshot/flameshot.ini rw,
  owner @{HOME}/.config/flameshot/#[0-9]*[0-9] rw,
  owner @{HOME}/.config/flameshot/flameshot.ini* rwl -> @{HOME}/.config/flameshot/#[0-9]*[0-9],
  owner @{HOME}/.config/flameshot/flameshot.ini.lock rwk,

  owner @{HOME}/.cache/ rw,
  owner @{HOME}/.cache/flameshot/ rw,
  owner @{HOME}/.cache/flameshot/** rw,

  # To configure Qt5/Qt6 settings (theme, font, icons, etc.) under DE/WM without Qt integration
  owner @{HOME}/.config/qt[0-9]*ct/ rw,
  owner @{HOME}/.config/qt[0-9]*ct/** rwkl -> @{HOME}/.config/qt[0-9]*ct/#[0-9]*[0-9],
  /usr/share/qt[0-9]*ct/** r,

  /var/lib/dbus/machine-id r,
  @{etc_ro}/machine-id r,

  /usr/share/hwdata/pnp.ids r,

  owner /tmp/.*/{,s} rw,
  owner /tmp/*= rw,
  owner /tmp/qipc_{systemsem,sharedmemory}_*[0-9a-f]* rw,

  deny owner @{PROC}/@{pid}/cmdline r,
  deny       @{PROC}/sys/kernel/random/boot_id r,
       owner @{PROC}/@{pid}/mountinfo r,
       owner @{PROC}/@{pid}/mounts r,

  @{etc_ro}/fstab r,

  /dev/shm/#[0-9]*[0-9] rw,

  # file_inherit
  owner /dev/tty[0-9]* rw,


  profile open {
    include <abstractions/base>
    include <abstractions/xdg-open>

    /{usr/,}bin/xdg-open                                    mr,
    /{usr/,}bin/exo-open                                    mr,
    /{usr/,}lib/@{multiarch}/glib-[0-9]*/gio-launch-desktop mr,

    /{usr/,}bin/{,ba,da}sh      rix,
    /{usr/,}bin/gawk            rix,
    /{usr/,}bin/readlink        rix,
    /{usr/,}bin/basename        rix,
    /{usr/,}bin/realpath        rix,

    owner @{HOME}/ r,

    owner @{run}/user/@{uid}/ r,

    # Allowed apps to open
    /{usr/,}lib/firefox/firefox rPUx,

    # file_inherit
    owner @{HOME}/.xsession-errors w,

  }

  include if exists <local/flameshot>
}
