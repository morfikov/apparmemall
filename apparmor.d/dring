# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}lib/ring/dring
profile dring @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>
  include <abstractions/audio>
  include <abstractions/video>

  network inet dgram,
  network inet6 dgram,
  network netlink raw,

  @{exec_path} mr,

  owner @{HOME}/.config/ring/ rw,
  owner @{HOME}/.config/jami/dring.yml rw,
  owner @{HOME}/.config/jami/dring.yml.bak w,
  owner @{HOME}/.local/share/jami/ r,

  @{sys}/class/ r,
  @{sys}/bus/ r,
  @{sys}/devices/system/node/ r,
  @{sys}/devices/system/node/node[0-9]*/meminfo r,

  /var/lib/dbus/machine-id r,
  @{etc_ro}/machine-id r,


  include if exists <local/dring>
}
