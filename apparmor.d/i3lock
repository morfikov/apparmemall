# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2020-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/i3lock
profile i3lock @{exec_path} {
  include <abstractions/base>
  include <abstractions/fonts>
  include <abstractions/fontconfig-cache-read>
  include <abstractions/nameservice-strict>
  include <abstractions/authentication>
  include <abstractions/wutmp>

  network netlink raw,

  @{exec_path} mr,

  owner @{HOME}/.Xauthority r,

  owner @{PROC}/@{pid}/fd/ r,

  # For background image.
  owner @{HOME}/*.png r,
  owner @{HOME}/*/*.png r,

  # When using also i3lock-fancy.
  owner /tmp/tmp.*.png r,

  # file_inherit
  owner /dev/tty[0-9]* rw,

  include if exists <local/i3lock>
}
