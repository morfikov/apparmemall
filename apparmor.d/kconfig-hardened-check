# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2020-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}bin/kconfig-hardened-check
@{exec_path} += /{usr/,}bin/kernel-hardening-checker
profile kconfig-hardened-check @{exec_path} {
  include <abstractions/base>
  include <abstractions/python>

  @{exec_path} r,

  @{etc_ro}/sysctl.conf r,
  /root/sysctl.txt r,

  # The usual kernel config locations
  /boot/config-* r,
  @{PROC}/config.gz r,

  # This is for kernels, which are built manually
  /**/.config r,

  # For checking the kernel command line parameters
  @{PROC}/cmdline r,

  include if exists <local/kconfig-hardened-check>
}
