# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/dmidecode
profile dmidecode @{exec_path} {
  include <abstractions/base>

  @{exec_path} mr,

  @{sys}/firmware/dmi/tables/smbios_entry_point r,
  @{sys}/firmware/dmi/tables/DMI r,

  # The following are needed when the --no-sysfs flag is used
  #capability sys_rawio,
  #/dev/mem r,
  #@{sys}/firmware/efi/systab r,

  # For dumping the output to a file
  owner /tmp/dump.bin rw,

  include if exists <local/dmidecode>
}
