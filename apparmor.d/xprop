# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}bin/xprop
profile xprop @{exec_path} {
  include <abstractions/base>

  @{exec_path} mr,

  owner @{HOME}/.Xauthority r,

  owner @{HOME}/.icons/default/index.theme r,
  /usr/share/icons/*/cursors/crosshair r,

  # file_inherit
  owner /dev/tty[0-9]* rw,
  owner @{HOME}/.xsession-errors w,

  include if exists <local/xprop>
}
