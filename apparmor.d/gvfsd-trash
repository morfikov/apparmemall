# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2021-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path}  = /{usr/,}lib/gvfs/gvfsd-trash
@{exec_path} += /usr/libexec/gvfsd-trash
profile gvfsd-trash @{exec_path} {
  include <abstractions/base>
  include <abstractions/nameservice-strict>
  include <abstractions/freedesktop.org>
  include <abstractions/trash>

  # When mounting a SMB share
  network inet stream,
  network inet6 stream,

  @{exec_path} mr,

  owner @{run}/user/@{uid}/gvfsd/socket-* rw,

  owner @{PROC}/@{pid}/mountinfo r,
  owner @{PROC}/@{pid}/mounts r,

  @{run}/mount/utab r,

  include if exists <local/gvfsd-trash>
}
