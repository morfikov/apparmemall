# vim:syntax=apparmor
# ------------------------------------------------------------------
#
#    Copyright (C) 2019-2025 Mikhail Morfikov <mmorfikov@gmail.com>
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
# ------------------------------------------------------------------

abi <abi/4.0>,

include <tunables/global>

@{exec_path} = /{usr/,}sbin/iw
profile iw @{exec_path} {
  include <abstractions/base>

  # To be able to manage network interfaces.
  capability net_admin,

  # Needed?
  audit deny capability sys_module,

  network netlink raw,

  @{exec_path} mr,

  @{sys}/devices/pci[0-9]*/**/ieee80211/phy[0-9]*/index r,

  # file_inherit
  owner /dev/tty[0-9]* rw,

  include if exists <local/iw>
}
